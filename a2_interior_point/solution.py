import numpy as np
import sys
import warnings

sys.path.append("..")
from optimization_algorithms.interface.nlp_solver import NLPSolver
from optimization_algorithms.interface.objective_type import OT


class SolverInteriorPoint(NLPSolver):

    def __init__(self, alpha = 1,rho_ls = 0.05, rho_alpha_plus = 1.2,rho_alpha_minus = 0.5, inner_tol = 1e-5,outer_tol = 1e-5, lambdaa = 1e-3,mu = 0.5,max_inner_iter = 25,max_outer_iter = 45,verbose = 0):
        """
        See also:
        ----
        NLPSolver.__init__
        """
        self.alpha = alpha
        self.rho_ls = rho_ls
        self.rho_alpha_plus = rho_alpha_plus
        self.rho_alpha_minus = rho_alpha_minus
        self.inner_tol = inner_tol
        self.outer_tol = outer_tol
        self.lambdaa = lambdaa
        self.max_inner_iter = max_inner_iter
        self.max_outer_iter = max_outer_iter
        self.mu = mu
        self.verbose = verbose

    def setProblem(self, problem):
        super().setProblem(problem)
        self.dim = self.problem.getDimension()
        types = np.array(self.problem.getFeatureTypes())
        self.index_f = np.nonzero(types == OT.f)[0]
        assert len(self.index_f) <= 1
        self.index_sos = np.nonzero(types == OT.sos)[0]
        self.index_eq = np.nonzero(types == OT.eq)[0]
        self.index_ineq = np.nonzero(types == OT.ineq)[0]

    def evaluate(self, x, trace=False):
        if trace:
            phi, J = self.problem.evaluate(x)
        else:
            phi, J = self.problem.mathematical_program.evaluate(x)
        fx = 0
        gradient = 0
        if len(self.index_f) > 0:
            fx += phi[self.index_f][0]
            gradient += J[self.index_f][0]
        if len(self.index_sos) > 0:
            fx += phi[self.index_sos].T @ phi[self.index_sos]
            gradient += 2 * J[self.index_sos].T @ phi[self.index_sos]
        if len(self.index_ineq) > 0:
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore")
                fx += -self.mu * np.sum(np.log(-phi[self.index_ineq]))
            div = np.divide(J[self.index_ineq], phi[self.index_ineq].reshape(-1,1))
            gradient += -self.mu * np.sum(div, axis=0)
        if np.isnan(fx):
            fx = np.inf
        return fx, gradient

    def calculate_lambda(self, H):
        try:
            eigvals = np.linalg.eigvals(H)
        except (np.linalg.LinAlgError, ValueError):
            lambdaa = self.lambdaa
        else:
            if not np.all(eigvals > 0):
                lambdaa = -np.amin(eigvals) + 1e-5
            else:
                lambdaa = self.lambdaa
        return lambdaa

    def getHessian(self, x, trace=False):
        if trace:
            phi, J = self.problem.evaluate(x)
        else:
            phi, J = self.problem.mathematical_program.evaluate(x)
        # ---------
        H = 0
        if len(self.index_f) > 0:
            H_f = self.problem.getFHessian(x)
            H += H_f
        if len(self.index_sos) > 0:
            H_sos = 2 * J[self.index_sos].T @ J[self.index_sos]
            H += H_sos
        if len(self.index_ineq) > 0:
            J_ineq = J[self.index_ineq]
            phi_ineq = phi[self.index_ineq]
            m = J_ineq.shape[0]
            outer = np.array([np.outer(J_ineq[i], J_ineq[i]) / phi_ineq[i]**2 for i in range(m)])
            H_ineq = np.sum(outer, axis=0)
            H += H_ineq
        return H


    def solve_inner(self, x):
        phi, _ = self.problem.mathematical_program.evaluate(x)
        if len(self.index_ineq) > 0:
            assert np.all(phi[self.index_ineq] <= 0)

        iteration = 1
        alpha = self.alpha
        lambdaa = self.lambdaa
        while iteration <= self.max_inner_iter:
            fx, gradient = self.evaluate(x, trace=True)
            try:
                H = self.getHessian(x)
                lambdaa = self.calculate_lambda(H)
                A = H + lambdaa * np.identity(self.dim)
                delta = np.linalg.solve(A, -gradient)
                if gradient.T @ delta > 0:
                    raise ValueError("Non-Descent")
            except (np.linalg.LinAlgError, ValueError):
                delta = -gradient / np.linalg.norm(gradient)
            step = alpha * delta
            fx_new, _ = self.evaluate(x + step)
            while fx_new > fx + self.rho_ls * gradient.T @ step:
                alpha *= self.rho_alpha_minus
                step = alpha * delta
                fx_new, _ = self.evaluate(x + step)
            step_norm = np.linalg.norm(step)
            x = x + step
            alpha = 1.0
            iteration += 1
            if step_norm <= self.inner_tol:
                break
        return x

    def solve(self):
        """

        See Also:
        ----
        NLPSolver.solve

        """
        iteration = 1
        x = self.problem.getInitializationSample()

        while iteration <= self.max_outer_iter:
            x_opt = self.solve_inner(x)
            step_norm = np.linalg.norm(x_opt - x)

            if step_norm <= self.outer_tol:
                break
            x = x_opt
            if self.mu < 1e-4:
                self.mu = 0
            else:
                self.mu *= 0.5
            iteration += 1
        return x_opt
