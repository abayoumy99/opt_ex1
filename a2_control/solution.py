import sys
import math
import numpy as np

sys.path.append("..")
from optimization_algorithms.interface.mathematical_program import MathematicalProgram
from optimization_algorithms.interface.objective_type import OT


class LQR(MathematicalProgram):
    """
    Parameters
    K integer
    A in R^{n x n}
    B in R^{n x n}
    Q in R^{n x n} symmetric
    R in R^{n x n} symmetric
    yf in R^n

    Variables
    y[k] in R^n for k=1,...,K
    u[k] in R^n for k=0,...,K-1

    Optimization Problem:
    LQR with terminal state constraint

    min 1/2 * sum_{k=1}^{K}   y[k].T Q y[k] + 1/2 * sum_{k=0}^{K-1}      u [k].T R u [k]
    s.t.
    y[1] - Bu[0]  = 0
    y[k+1] - Ay[k] - Bu[k] = 0  ; k = 1,...,K-1
    y[K] - yf = 0

    Hint: Use the optimization variable:
    x = [ u[0], y[1], u[1],y[2] , ... , u[K-1], y[K] ]

    Use the following features:
    1 - a single feature of types OT.f
    2 - the features of types OT.eq that you need
    """

    def __init__(self, K, A, B, Q, R, yf):
        """
        Arguments
        -----
        T: integer
        A: np.array 2-D
        B: np.array 2-D
        Q: np.array 2-D
        R: np.array 2-D
        yf: np.array 1-D
        """
        # in case you want to initialize some class members or so...
        n = len(yf)
        self.Q = Q
        self.R = R
        self.yf = yf
        self.K = K
        self.A = A
        self.B = B
        self.n = n
        self.H_dn = np.zeros((K * n, 2 * K * n))
        for t in range(K):
            cur = (n + 1) * t
            if t > 0:
                self.H_dn[n * t:n * t + n, 2 * n * t - n:2 * n * t] = -A
            self.H_dn[n * t:n * (t + 1), 2 * n * t:2 * n * t + n] = -B
            self.H_dn[n * t:n * (t + 1), 2 * n * t + n:2 * n * t + 2 * n] = np.eye(n)

    def evaluate(self, x):
        """
        See also:
        ----
        MathematicalProgram.evaluate
        """
        NewX = x.reshape((2 * self.K, self.n))
        f = 0
        u = NewX[::2]
        y = NewX[1::2]

        Jf = np.zeros((2 * self.n * self.K))

        for i in range(self.K):
            f += 1 / 2 * u[i].T @ self.R @ u[i] + 1 / 2 * y[i].T @ self.Q @ y[i]
            Jf[2 * i * self.n:(2 * i + 1) * self.n] = self.R @ u[i]
            Jf[(2 * i + 1) * self.n:(2 * i + 2) * self.n] = self.Q @ y[i]

        Jf = np.reshape(Jf, (1, -1))
        eq_dyn = self.H_dn @ x
        J_dyn = self.H_dn
        eq_des = y[self.K - 1, :] - self.yf
        J_des = np.zeros((self.n, 2 * self.K * self.n))
        J_des[:, -self.n:] = np.eye(self.n)
        phi = np.concatenate(([f], eq_dyn, eq_des))
        J = np.concatenate([Jf, J_dyn, J_des], axis=0)

        return phi, J

    def getFHessian(self, x):
        """
        """
        H = np.zeros((self.K * 2 * self.n, self.K * 2 * self.n))

        for i in range(self.K):
            H[2 * i * self.n:2 * i * self.n + self.n, 2 * i * self.n:2 * i * self.n + self.n] = self.R
            H[2 * i * self.n + self.n:2 * i * self.n + 2 * self.n,
            2 * i * self.n + self.n:2 * i * self.n + 2 * self.n] = self.Q

        return H

    def getDimension(self):
        """
        See Also
        ------
        MathematicalProgram.getDimension
        """
        return 2 * self.n * self.K

    def getInitializationSample(self):
        """
        See Also
        ------
        MathematicalProgram.getInitializationSample
        """
        return np.zeros(self.getDimension())

    def getFeatureTypes(self):
        """
        returns
        -----
        output: list of feature Types
        See Also
        ------
        MathematicalProgram.getFeatureTypes
        """
        return [OT.f] + [OT.eq] * ((self.K + 1) * self.n)
