import sys
import math
import numpy as np

sys.path.append("..")
from optimization_algorithms.interface.mathematical_program import MathematicalProgram
from optimization_algorithms.interface.objective_type import OT


class RobotTool(MathematicalProgram):
    """
    """

    def __init__(self, q0, pr, l):

        self.q0 = q0
        self.pr = pr
        self.l = l


    def evaluate(self, q):
        # phi1 ---------------------------------------------------------------
        q1, q2, q3 = q
        p1 = np.cos(q1) + (1/2)*np.cos(q1+q2) + (1/3)*np.cos(q1+q2+q3)
        p2 = np.sin(q1) + (1/2)*np.sin(q1+q2) + (1/3)*np.sin(q1+q2+q3)
        p = np.array([p1,p2])
        phi1 = p - self.pr

        dp1_q1 = -p2
        dp1_q2 = (-1/2)*np.sin(q1+q2) + (-1/3)*np.sin(q1+q2+q3)
        dp1_q3 = (-1/3)*np.sin(q1+q2+q3)

        dp2_q1 = p1
        dp2_q2 = (1/2)*np.cos(q1+q2) + (1/3)*np.cos(q1+q2+q3)
        dp2_q3 = (1/3)*np.cos(q1+q2+q3)

        J_phi1 = np.array([[dp1_q1, dp1_q2, dp1_q3], [dp2_q1, dp2_q2, dp2_q3]])
        grad_f1 = 2 * J_phi1.T @ phi1
        #print (grad_f1)
        # phi2 ---------------------------------------------------------------
        phi2 = np.sqrt(self.l) * (q-self.q0)
        #print(self.l)
        J_phi2 = np.sqrt(self.l) * np.eye(3)
        print(J_phi2.T)
        grad_f2 = 2 * J_phi2.T @ phi2

        phi = np.hstack((phi1,phi2))

        J = np.vstack((J_phi1, J_phi2))

        return phi, J

    def getDimension(self):
        """
        See Also
        ------
        MathematicalProgram.getDimension
        """
        # return the input dimensionality of the problem (size of x)
        return self.q0.size

    def getInitializationSample(self):
        """
        See Also
        ------
        MathematicalProgram.getInitializationSample
        """
        return self.q0

    def getFeatureTypes(self):
        """
        returns
        -----
        output: list of feature Types
        See Also
        ------
        MathematicalProgram.getFeatureTypes
        """
        return [OT.sos] * 5
