import math
import sys
import numpy as np

sys.path.append("..")
from optimization_algorithms.interface.mathematical_program import MathematicalProgram
from optimization_algorithms.interface.objective_type import OT


class AntennaPlacement(MathematicalProgram):
    """
    """

    def __init__(self, P, w):
        """
        Arguments
        ----
        P: list of 1-D np.arrays
        w: 1-D np.array
        """
        # in case you want to initialize some class members or so...
        self.m = len(w)
        self.P = P
        self.w = w

    def evaluate(self, x):
        """
        See also:
        ----
        MathematicalProgram.evaluate
        """
        y = 0
        J = np.zeros((2,))
        for i in range(self.m):
            y = y + self.w[i]*np.exp(-np.linalg.norm(x-self.P[i])**2)
            J = J + self.w[i]*np.exp(-np.linalg.norm(x-self.P[i])**2)*2*(x-self.P[i])
        y = y*-1
        return np.array([y]) , np.array([J])

    def getDimension(self):
        """
        See Also
        ------
        MathematicalProgram.getDimension
        """
        # return the input dimensionality of the problem (size of x)
        return self.m

    def getFHessian(self, x):
        """
        See Also
        ------
        MathematicalProgram.getFHessian
        """
        H = np.zeros((2,2))
        for i in range(self.m):
            exp = np.exp(-np.linalg.norm(x-self.P[i])**2)
            H[0,0] = H[0,0]+self.w[i]*(2*exp - 2*x[0]*2*(x[0]-self.P[i][0])*exp +2*self.P[i][0]*2*(x[0]-self.P[i][0])*exp)
            H[1,1] = H[1,1]+self.w[i]*(2*exp - 2*x[1]*2*(x[1]-self.P[i][1])*exp +2*self.P[i][1]*2*(x[1]-self.P[i][1])*exp)
            H[1,0] = H[1,0]+self.w[i]*(-2*x[0]*2*(x[1]-self.P[i][1])*exp+2*self.P[i][0]*2*(x[1]-self.P[i][1])*exp)
            H[0,1] = H[0,1]+self.w[i]*(-2*x[1]*2*(x[0]-self.P[i][0])*exp+2*self.P[i][1]*2*(x[0]-self.P[i][0])*exp)
        return H

    def getInitializationSample(self):
        """
        See Also
        ------
        MathematicalProgram.getInitializationSample
        """
        x0 = 1/self.m * np.sum(self.P, axis=0)
        return x0

    def getFeatureTypes(self):
        """
        returns
        -----
        output: list of feature Types

        """
        return [OT.f]
