import numpy as np
import sys
import time

sys.path.append("..")
from optimization_algorithms.interface.nlp_solver import NLPSolver
from optimization_algorithms.interface.objective_type import OT


class SolverAugmentedLagrangian(NLPSolver):

    def __init__(self, theta=1e-5,eps = 1e-5 ,mu = 10,ny = 10 ,rho_mu_p = 1.6,rho_ny_p = 1.6,nQueries = 0,maxQueries = 1e6 ,maxTime = 1e6,alpha = 1,rho_m_alpha = 1,innerTol = 1e-5,rho_m = 0.5,rho_p = 1.2,
                 delta_max = np.Inf,rho_ls = 0.01, lambdaa = 1e-5,rho_p_lambda = 1.2,rho_m_lambda = 0.5 ,maxInnerIter = 1500,FHessianImplemented = False):
        """
        See also:
        ----
        NLPSolver.__init__
        """
        self.theta =theta
        self.eps = eps
        self.mu = mu
        self.ny = ny
        self.rho_mu_p = rho_mu_p
        self.rho_ny_p = rho_ny_p
        self.nQueries = nQueries
        self.maxQueries = maxQueries
        self.maxTime = maxTime
        self.alpha = alpha
        self.rho_m_alpha = rho_m_alpha
        self.innerTol = innerTol
        self.rho_m = rho_m
        self.rho_p = rho_p
        self.delta_max = delta_max
        self.rho_ls = rho_ls
        self.lambdaa = lambdaa
        self.rho_p_lambda = rho_p_lambda
        self.rho_m_lambda = rho_m_lambda
        self.maxInnerIter = maxInnerIter
        self.FHessianImplemented = FHessianImplemented

    def setProblem(self, problem):
        super().setProblem(problem)
        self.n = self.problem.getDimension()
        types = np.array(self.problem.getFeatureTypes())
        self.index_f = np.nonzero(types == OT.f)[0]
        self.index_sos = np.nonzero(types == OT.sos)[0]
        self.index_ineq = np.nonzero(types == OT.ineq)[0]
        self.index_eq = np.nonzero(types == OT.eq)[0]
        types = self.problem.getFeatureTypes()
        self.N_fet = len(types)
        self.kappa  = np.zeros((self.N_fet,1))
        self.lambd_vec = np.zeros((self.N_fet, 1))

    def evaluate(self, x):
        self.nQueries += 1
        if self.nQueries >= self.maxQueries:
            raise RuntimeError('Max queries reached')
        phi, J = self.problem.mathematical_program.evaluate(x)
        cost = 0
        grad = 0
        if len(self.index_f) > 0:
            cost += phi[self.index_f][0]
            grad += J[self.index_f][0]
        if len(self.index_sos) > 0:
            cost += phi[self.index_sos].T @ phi[self.index_sos]
            grad += 2 * J[self.index_sos].T @ phi[self.index_sos]

        if len(self.index_ineq) > 0:

            for ineqIndex in self.index_ineq:
                g, dg, lam = phi[ineqIndex], J[ineqIndex], self.lambd_vec[ineqIndex][0]
                cost += (g >= 0 or lam > 0) * self.ny * g ** 2 + lam * g
                grad += (2 * (g >= 0 or lam > 0) * self.ny * g + lam) * dg
        if len(self.index_eq) > 0:

            for eqIndex in self.index_eq:
                h, dh, kappa = phi[eqIndex], J[eqIndex], self.kappa[eqIndex][0]
                cost += self.mu * h ** 2 + kappa * h
                grad += 2 * self.mu * h * dh + kappa * dh
        return cost, grad, phi

    def calculate_lambda(self, H):
        EVs = np.linalg.eigvals(H)
        if not np.all(EVs > 0):
            lambdaa = -np.amin(EVs) + self.lambdaa
        else:
            lambdaa = self.lambdaa
        return lambdaa

    def getHessian(self, x):
        phi, J = self.problem.evaluate(x)

        H = 0
        if len(self.index_f) > 0 and self.FHessianImplemented:
            H += self.problem.getFHessian(x)
        if len(self.index_sos) > 0:
            H += 2 * J[self.index_sos].T @ J[self.index_sos]
        if len(self.index_ineq) > 0:
            for ineqIndex in self.index_ineq:
                g, dg, lam = phi[ineqIndex], J[ineqIndex], self.lambd_vec[ineqIndex]
                H += 2 * self.ny * np.outer(dg, dg) * (g >= 0 or lam > 0)
        if len(self.index_eq) > 0:
            for eqIndex in self.index_eq:
                dh = J[eqIndex]
                H += 2 * self.mu * np.outer(dh, dh)
        return H

    def solver_inner(self, x):
        phi, _ = self.problem.mathematical_program.evaluate(x)
        alpha = self.alpha
        lambdaa = self.lambdaa
        innerIter = 1
        step_norm = np.Inf
        while step_norm >= self.innerTol and (innerIter < self.maxInnerIter):
            cost, grad, _ = self.evaluate(x)
            H = self.getHessian(x)
            A = H + lambdaa * np.eye(self.n)
            delta = -np.linalg.inv(A) @ grad
            if grad.T @ delta > 0:
                delta = -grad / np.linalg.norm(grad)
            step = alpha * delta
            cost_new, _, phi = self.evaluate(x + step)
            while cost_new > cost + self.rho_ls * grad.T @ step:
                alpha *= self.rho_m
                step = alpha * delta
                cost_new, _, phi = self.evaluate(x + step)
            step_norm = np.linalg.norm(step)

            x = x + step
            alpha = min(self.rho_p * alpha, 1)
            innerIter += 1
        for ineq in self.index_ineq:
            self.lambd_vec[ineq] = np.maximum(self.lambd_vec[ineq] + 2 * self.ny * phi[ineq], 0)
        for eq in self.index_eq:
            self.kappa[eq] = self.kappa[eq] + 2 * self.mu * phi[eq]
        self.mu *= self.rho_mu_p
        self.ny *= self.rho_ny_p
        return x, phi

    def solve(self):
        startTime = time.perf_counter()
        x = self.problem.getInitializationSample()
        x_prev = x + 100 * self.theta
        rpt = 0
        try:
            H = self.problem.getFHessian(x)
            self.FHessianImplemented = True
        except:
            self.FHessianImplemented = False

        while time.perf_counter() - startTime < self.maxTime:
            x, phi = self.solver_inner(x)
            self.alpha *= self.rho_m_alpha
            if len(self.index_ineq) > 0:
                maxG = np.amax(phi[self.index_ineq])
            else:
                maxG = 0
            if len(self.index_eq) > 0:
                maxH = np.amax(abs(phi[self.index_eq]))
            else:
                maxH = 0
            if np.linalg.norm(x - x_prev) < self.theta and max(maxG, maxH) < self.eps:

                if rpt >= 1:
                    print('I converged successfully!! Total number of queries:', self.nQueries)
                    break
                rpt += 1
            else:
                rpt = 0
            x_prev = x

        return x

