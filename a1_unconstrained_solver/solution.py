import numpy as np
import sys

sys.path.append("..")
from optimization_algorithms.interface.nlp_solver import NLPSolver
from optimization_algorithms.interface.objective_type import OT


class SolverUnconstrained(NLPSolver):

    def __init__(self, **kwargs):
        """
        See also:
        ----
        NLPSolver.__init__
        """

        # in case you want to initialize some class members or so...
        self.kwargs = kwargs
        self.verbose = self.kwargs.get("verbose", 0)   
    
    def setProblem(self, problem):
        super().setProblem(problem)
        self.dim = self.problem.getDimension()
        types = self.problem.getFeatureTypes()
        # get all features of type f
        self.index_f = [i for i, x in enumerate(types) if x == OT.f]
        assert len(self.index_f) <= 1 # at most, only one term of type OT.f
        # get all sum-of-square features
        self.index_r = [i for i, x in enumerate(types) if x == OT.sos]
    
    def evaluate(self, x, trace=False):
        if trace:
            phi, J = self.problem.evaluate(x)
        else:
            phi, J = self.problem.mathematical_program.evaluate(x)        
        c = 0
        gradient = 0
        if len(self.index_f) > 0:
            c += phi[self.index_f][0]
            gradient += J[self.index_f][0]
        if len(self.index_r) > 0:
            c += phi[self.index_r].T @ phi[self.index_r]  
            gradient += 2 * J[self.index_r].T @ phi[self.index_r]
        return c, gradient
    
    def getHessian(self, x, trace=False):
        H = 0
        if len(self.index_f) > 0:
            H += self.problem.getFHessian(x)
        if len(self.index_r) > 0:
            if trace:
                phi, J = self.problem.evaluate(x)
            else:
                phi, J = self.problem.mathematical_program.evaluate(x)
            H += 2 * J[self.index_r].T @ J[self.index_r]
        return H
    
    def calculate_lambda(self, H):
        try:
            eigvals = np.linalg.eigvals(H)
        except (np.linalg.LinAlgError, ValueError):
            lamda = self.kwargs.get("lambda", 1e-3)
        else:
            if not np.all(eigvals > 0):
                lamda = -np.amin(eigvals) + 1e-5
            else:
                lamda = 0
        return lamda
        
    
    def solve(self):
        # Parameters ---------------------------------------------------------
        # stepsize
        alpha = self.kwargs.get("alpha", 1)         
        # line search factor, generally in interval [0.01, 0.3]
        rho_ls = self.kwargs.get("rho_ls", 0.05)         
        # increase_factor
        rho_alpha_plus = self.kwargs.get("rho_alpha_plus", 1.2)        
        # line search decrease_factor
        rho_alpha_minus = self.kwargs.get("rho_alpha_minus", 0.5)
        # tolerance  
        theta = self.kwargs.get("theta", 1e-3)
        # damping
        lamda = self.kwargs.get("lambda", 1e-3) # previous 1e-3
        # metric        
        metric = self.kwargs.get("metric", np.identity(self.dim))
        # max iterations
        max_iter = self.kwargs.get("max_iter", 20)
        
        # Initialization -----------------------------------------------------
        iteration = 0
        x = self.kwargs.get("x_init", self.problem.getInitializationSample())
        fx, gradient = self.evaluate(x, trace=True)
        step_norm = np.sum(np.linalg.norm(np.inf - x, ord=2))
        if self.dim <=3 and self.verbose:
            print("\n------------------------------------------------------------------------")
            print(f"iteration = {iteration} \nx_init = {x} \nfx_init = {fx:.4f} \nstep_norm = {step_norm:.5f}")
        
        # Optimization Loop: Newton Method + Gradient Descent fallback -------
        while step_norm >= theta and iteration<=max_iter:            
            try:
                H = self.getHessian(x)                
                # lamda = self.calculate_lambda(H) # decreases nb of iterations until convergence
                A = H + lamda * np.identity(self.dim)
                b = - gradient
                delta = np.linalg.solve(A, b) # == -H.inv @ grad
            except np.linalg.LinAlgError:
                # non-convex: ill-defined linear system
                delta = -gradient / np.linalg.norm(gradient, ord=2) 
            else:
                # non-descent
                if gradient.T @ delta > 0:                        
                    delta = -gradient / np.linalg.norm(gradient, ord=2)

            step = alpha * np.linalg.inv(metric) @ delta
            fx_new, _ = self.evaluate(x + step)
            
            # linesearch + backtracking
            while fx_new > fx + rho_ls * gradient.T @ step:
                alpha = rho_alpha_minus * alpha
                step = alpha * np.linalg.inv(metric) @ delta
                fx_new, _ = self.evaluate(x + step)
                                       
            x = x + step
            # alpha = rho_alpha_plus * alpha
            alpha = 1
            iteration += 1
            step_norm = np.sum(np.linalg.norm(step, ord=2))            
            fx, gradient = self.evaluate(x, trace=True)
            
            if self.verbose == 2:
                self.print_progress(iteration, x, fx, gradient, step, step_norm)
        
        if self.verbose == 1:
            self.print_progress(iteration, x, fx, gradient, step, step_norm)            
        
        return x
    
    def print_progress(self, iteration, x, fx, gradient, step, step_norm):        
        if self.dim <= 3:
            print(f"\niteration = {iteration}")
            print(f"x = {x}")
            print(f"fx = {fx:.4f}")
            print(f"gradient = {gradient}")  
            print(f"step = {step}")
            print(f"step_norm = {step_norm:.5f}")            
        else:
            print(f"\niteration = {iteration}")
            print(f"fx = {fx:.4f}")
            print(f"step_norm = {step_norm:.5f}")

